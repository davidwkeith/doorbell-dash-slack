#!/usr/bin/env node

const dash_button = require('node-dash-button');
const moment = require('moment');
const program = require('commander');
const Slack = require('slack-node');
const packageJSON = require('./package.json');

require('dotenv').config({path: '.env.local'});
require('dotenv').config({path: '.env'});


program
  .version(packageJSON.version)
  .option('-m, --mac-address [macAddress]', 'Dash button MAC address', process.env.DDS_DASH_MAC_ADDRESS)
  .option('-w, --web-hook [webhookURL]', 'Slack webhook URL', process.env.DDS_SLACK_WEBHOOK_URL)
  .option('-c, --channel [channelName]', 'Slack channel to notify', process.env.DDS_SLACK_CHANNEL)
  .option('-u, --username [username]', 'Slack username', process.env.DDS_SLACK_USERNAME)
  .option('-i, --icon [iconEmoji]', 'Slack icon emoji', process.env.DDS_SLACK_ICON_EMOJI)
  .option('-s, --slack-message [slackMessage]', 'Slack message', process.env.DDS_SLACK_MESSAGE) 
  .parse(process.argv);


const dash = dash_button(program.macAddress);
const slack = new Slack();
slack.setWebhook(program.webhookURL);

dash.on('detected', () => {
  slack.webhook({
    channel: program.channelName,
    username: program.username,
    icon_emoji: program.iconEmoji,
    text: [moment().format('h:mm:ss a'), program.slackMessage].join(' ')
  }, (err) => {
    if (err) console.log(err);
  });
}); 