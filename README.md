# Doorbell Dash Slack

Use an Amazon Dash Button as a doorbell and send the notifications to a Slack channel.

## Usage

```bash
Usage: doorbell-dash-slack [options]

Options:
  -V, --version                       output the version number
  -m, --mac-address [macAddress]      Dash button MAC address
  -w, --web-hook [webhookURL]         Slack webhook URL
  -c, --channel [channelName]         Slack channel to notify (default: "#doorbell")
  -u, --username [username]           Slack username (default: "Doorbell")
  -i, --icon [iconEmoji]              Slack icon emoji (default: ":bell:")
  -s, --slack-message [slackMessage]  Slack message (default: "Someone is at the door")
  -h, --help                          output usage information
```

Defaults are pulled in from the following env variables is not provided on the command line:

* `DDS_AMAZON_DASH_MAC` macAddress
* `DDS_SLACK_URL` webhookURL
* `DDS_SLACK_CHANNEL` channelName
* `DDS_SLACK_USERNAME` username
* `DDS_SLACK_ICON_EMOJI` iconEmoji
* `DDS_SLACK_MESSAGE` slackMessage

## Run as service on Raspberry Pi

Create the following file in `/etc/systemd/system/doorbell-dash-slack.service`:

```text
[Unit]
Description=doorbell-dash-slack
After=network.target

[Service]
ExecStart=doorbell-dash-slack
WorkingDirectory=/home/pi/node_modules/.bin/doorbell-dash-slack/
StandardOutput=inherit
StandardError=inherit
Restart=always
User=root

[Install]
WantedBy=multi-user.target
```

Then just run `sudo systemctl start doorbell-dash-slack.service`.

Similar services exist for other operating systems.

## Making a Dash Button look like a 'normal' doorbell

See `./img/dashdoorbell.pdf`

Ultimately we just used Elmer's Glue to stick this on, but you could go to a professionaly print shop to get a glossy sticker done up if you want it to look really good.

[Bell Icon from Iconfinder](https://www.iconfinder.com/icons/211694/bell_icon)
